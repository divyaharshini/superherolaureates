//
//  SuperHeros.swift
//  superheroesandlaureates
//
//  Created by Bheemireddy, Divyaharshini on 4/13/19.
//  Copyright © 2019 Bheemireddy, Divyaharshini. All rights reserved.
//

import Foundation
class SuperHeroes{
    
    static let shared = SuperHeroes()
    var members:[Members] = []
    func fetchSuperHero() -> Void {
        guard let url = Bundle.main.url(forResource: "squad", withExtension: "json")
            else{
                print("File not Found")
                return
            }
        let urlSession = URLSession.shared
        urlSession.dataTask(with: url, completionHandler: superHeroDetails).resume()
    }
    
    func superHeroDetails(data:Data?, urlResponse:URLResponse?, error:Error?) -> Void {
        do {
            let decoder = JSONDecoder()
            let superHero = try decoder.decode(SuperHero.self, from: data!)
            members = superHero.members
            NotificationCenter.default.post(name: Notification.Name("Heroes Retrieved"), object: nil)
        }
        catch {
            print(error)
        }
    }
    func heroes(index:Int) -> Members{
        return members[index]
    }
    subscript(index:Int) -> Members{
        return members[index]
    }
    func numofLaureates() -> Int{
        return members.count
    }
}

class SuperHero: Codable {
    var members:[Members]
}

class Members: Codable{
    var name:String
    var secretIdentity:String
    var powers:[String]
}
